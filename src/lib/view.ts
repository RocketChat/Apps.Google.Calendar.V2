import {
    IPersistence,
    IRead,
} from '@rocket.chat/apps-engine/definition/accessors';
import {
    RocketChatAssociationModel,
    RocketChatAssociationRecord,
} from '@rocket.chat/apps-engine/definition/metadata';
import {
    ButtonStyle,
    IBlock,
    IButtonElement,
    ISectionBlock,
} from '@rocket.chat/apps-engine/definition/uikit';
import {
    IUIKitContextualBarViewParam,
    IUIKitModalViewParam,
} from '@rocket.chat/apps-engine/definition/uikit/UIKitInteractionResponder';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../GoogleCalendarApp';
import { IMessageButonActions } from '../../IGoogleCalendarApp';

export class ViewController {
    private app: GoogleCalendarApp;
    private read: IRead;
    private persistence: IPersistence;
    private association: RocketChatAssociationRecord;
    private cachedAssociation: RocketChatAssociationRecord;
    private viewId: string;
    private user: IUser;
    constructor(
        app: GoogleCalendarApp,
        persistence: IPersistence,
        read: IRead,
        viewId: string,
        user: IUser,
    ) {
        this.app = app;
        this.read = read;
        this.persistence = persistence;
        this.association = new RocketChatAssociationRecord(
            RocketChatAssociationModel.MISC,
            viewId,
        );
        this.cachedAssociation = new RocketChatAssociationRecord(
            RocketChatAssociationModel.MISC,
            'viewCache',
        );
        this.viewId = viewId;
        this.user = user;
    }

    public async create(view: IUIKitContextualBarViewParam): Promise<void> {
        await this.persistence.createWithAssociations(view, [
            this.cachedAssociation,
            this.association,
        ]);
    }

    public async updateViewBlock(
        newBlock: ISectionBlock,
        blockId: string,
    ): Promise<IUIKitContextualBarViewParam> {
        const viewToUpdate = await this.getViewById();

        if (!viewToUpdate) {
            throw new Error(`There is no view stored by the id ${this.viewId}`);
        }

        const idx = viewToUpdate.blocks.findIndex(
            (block: IBlock) => (block.blockId as string) === blockId,
        );

        viewToUpdate.blocks[idx] = newBlock;

        await this.persistence.updateByAssociations(
            [this.cachedAssociation, this.association],
            viewToUpdate,
        );

        return viewToUpdate;
    }

    public async getViewById(): Promise<IUIKitContextualBarViewParam> {
        const [record] = await this.read
            .getPersistenceReader()
            .readByAssociations([this.cachedAssociation, this.association]);

        return record as IUIKitContextualBarViewParam;
    }

    public async toggleButtonsView(
        blockId: string,
    ): Promise<IUIKitContextualBarViewParam> {
        const view = await this.getViewById();
        const idx = view.blocks.findIndex(
            // tslint:disable-next-line: no-shadowed-variable
            (block: IBlock) => (block.blockId as string) === blockId,
        );

        const block = view.blocks[idx] as ISectionBlock;
        const blockButton = block.accessory as IButtonElement;

        const oauth2ClientInstance = this.app.getOauth2ClientInstance();

        const authURL = (
            await oauth2ClientInstance.getUserAuthorizationUrl(this.user)
        ).href;

        if (blockButton.actionId === IMessageButonActions.RemoveNotifications) {
            blockButton.actionId = IMessageButonActions.AddNotifications;
            blockButton.text.text = 'Activate';
            blockButton.style = ButtonStyle.PRIMARY;
        } else if (
            blockButton.actionId === IMessageButonActions.AddNotifications
        ) {
            blockButton.actionId = IMessageButonActions.RemoveNotifications;
            blockButton.text.text = 'Remove';
            blockButton.style = ButtonStyle.DANGER;
        } else if (
            blockButton.actionId === IMessageButonActions.RevokeGoogleOAuth
        ) {
            blockButton.actionId = IMessageButonActions.AuthGoogleOAuth;
            blockButton.text.text = 'Authorize';
            blockButton.style = ButtonStyle.PRIMARY;
            blockButton.url = authURL;
        } else if (
            blockButton.actionId === IMessageButonActions.AuthGoogleOAuth
        ) {
            blockButton.actionId = IMessageButonActions.RevokeGoogleOAuth;
            blockButton.text.text = 'Revoke';
            blockButton.style = ButtonStyle.DANGER;
        }

        const updated = await this.updateViewBlock(
            block,
            block.blockId as string,
        );

        return updated;
    }
}
