import { ITodayEventList } from "../definitions/Events";

export function formatEventsForToday(events: ITodayEventList): string {
    return `${events.title} \n ${events.list
        .map((list) => `${list.start} - ${list.end} - ${list.summary}\n`)
        .join('')}`;
}
