import {
    IModify,
    IPersistence,
    IRead,
} from '@rocket.chat/apps-engine/definition/accessors';
import {
    AccessoryElements,
    BlockBuilder,
    BlockElementType,
    ButtonStyle,
} from '@rocket.chat/apps-engine/definition/uikit';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../GoogleCalendarApp';
import { IMessageButonActions } from '../../IGoogleCalendarApp';
import { ViewController } from './view';

function getAccessory(
    isAlreadyAuth: boolean,
    block: BlockBuilder,
    authURL: string,
): AccessoryElements {
    if (isAlreadyAuth) {
        return {
            type: BlockElementType.BUTTON,
            actionId: IMessageButonActions.RevokeGoogleOAuth,
            text: block.newPlainTextObject('Revoke'),
            style: ButtonStyle.DANGER,
        };
    }

    return {
        type: BlockElementType.BUTTON,
        actionId: IMessageButonActions.AuthGoogleOAuth,
        text: block.newPlainTextObject('Authorize'),
        style: ButtonStyle.PRIMARY,
        url: authURL,
    };
}

export async function contextualBarOptions({
    id = '',
    read,
    modify,
    persistence,
    app,
    user,
}: {
    id?: string;
    read: IRead;
    modify: IModify;
    persistence: IPersistence;
    app: GoogleCalendarApp;
    user: IUser;
}): Promise<any> {
    const viewController = new ViewController(app, persistence, read, id, user);

    const view = await viewController.getViewById();
    if (!view) {
        const authURL = (
            await app.getOauth2ClientInstance().getUserAuthorizationUrl(user)
        ).href;

        const isAlreadyAuth = !!(await app
            .getOauth2ClientInstance()
            .getAccessTokenForUser(user));

        const block = modify.getCreator().getBlockBuilder();
        block.addSectionBlock({
            text: block.newMarkdownTextObject(
                '*Notifications* \n' +
                    'You can turn on/off the individual Google Calendar events message notifications.'
            ),
            accessory: {
                type: BlockElementType.BUTTON,
                actionId: IMessageButonActions.RemoveNotifications,
                text: block.newPlainTextObject('Remove'),
                style: ButtonStyle.DANGER,
            },
        });

        block.addSectionBlock({
            text: block.newMarkdownTextObject(
                '*OAuth Authorization* \n' +
                    'If you revoke the authorization, the app will stop to send you messages regarding the Google Calendar updates.'
            ),
            accessory: { ...getAccessory(isAlreadyAuth, block, authURL) },
        });

        await viewController.create({
            id,
            title: block.newPlainTextObject('Google Calendar Settings'),
            blocks: block.getBlocks(),
        });

        return {
            id,
            title: block.newPlainTextObject('Google Calendar Settings'),
            blocks: block.getBlocks(),
        };
    }

    return view;
}
