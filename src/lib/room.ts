import { IModify, IRead } from '@rocket.chat/apps-engine/definition/accessors';
import { IRoom, RoomType } from '@rocket.chat/apps-engine/definition/rooms';
import { GoogleCalendarApp } from '../../GoogleCalendarApp';

/**
 * Gets a direct message room between bot and another user, creating if it doesn't exist
 *
 * @param app GoogleCalendarApp
 * @param read
 * @param modify
 * @param username the username to create a direct with bot
 * @returns the room or undefined if botUser or botUsername is not set
 */
export async function getDirect(
    app: GoogleCalendarApp,
    read: IRead,
    modify: IModify,
    username: string,
): Promise<IRoom | undefined> {
    if (app.botUsername) {
        const usernames = [app.botUsername, username];
        const room = await read.getRoomReader().getDirectByUsernames(usernames);
        if (room) {
            return room;
        } else if (app.botUser) {
            // Create direct room between botUser and username
            const newRoom = modify
                .getCreator()
                .startRoom()
                .setType(RoomType.DIRECT_MESSAGE)
                .setCreator(app.botUser)
                .setMembersToBeAddedByUsernames(usernames);
            const roomId = await modify.getCreator().finish(newRoom);
            return await read.getRoomReader().getById(roomId);
        }
    }
    return;
}
