import { IModify, IPersistence, IRead } from '@rocket.chat/apps-engine/definition/accessors';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../../GoogleCalendarApp';
import { createSectionBlock, IButton } from '../../lib/blocks';
import { sendDirectMessage } from '../../lib/message';

export async function authorize(app: GoogleCalendarApp, read: IRead, modify: IModify, user: IUser, persistence: IPersistence): Promise<void> {
    const url = await app.getOauth2ClientInstance().getUserAuthorizationUrl(user);

    const button: IButton = {
        text: 'Authorize',
        url: url.toString(),
    };

    // @TODO better copy
    const message = 'Click the button to authorize your account';

    const block = await createSectionBlock(modify, message, button);

    await sendDirectMessage(read, modify, user, '', persistence, block);
}
