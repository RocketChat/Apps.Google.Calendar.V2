import { IModify, IPersistence, IRead } from '@rocket.chat/apps-engine/definition/accessors';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../../GoogleCalendarApp';
import { sendDirectMessage } from '../../lib/message';

export async function revoke(app: GoogleCalendarApp, read: IRead, modify: IModify, persistence: IPersistence, user: IUser): Promise<void> {
    try {
        await app.getOauth2ClientInstance().revokeUserAccessToken(user, persistence);
    } catch (error) {
        const errorMessage = `Failed to revoke user credentials for the Google Calendar App\ Details: ${JSON.stringify(error)}`;
        app.getLogger().error(errorMessage);
        return;
    }

    // @TODO better copy
    const message = 'Your credentials for the Google Calendar App had been revoked.';

    await sendDirectMessage(read, modify, user, message, persistence);
}
