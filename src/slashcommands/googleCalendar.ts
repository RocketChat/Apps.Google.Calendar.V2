import {
    IHttp,
    IModify,
    IPersistence,
    IRead,
} from '@rocket.chat/apps-engine/definition/accessors';
import { IRoom } from '@rocket.chat/apps-engine/definition/rooms';
import {
    ISlashCommand,
    SlashCommandContext,
} from '@rocket.chat/apps-engine/definition/slashcommands';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../GoogleCalendarApp';
import { Subcommands } from '../enums/Subcommands';
import { sendNotification } from '../lib/message';
import { checkForUserEvents } from '../processors/checkForUserEvents';
import { authorize } from './subcommands/authorize';
import { revoke } from './subcommands/revoke';

export class GoogleCalendar implements ISlashCommand {
    public command = 'google-calendar';
    public i18nParamsExample = 'slashcommand_params';
    public i18nDescription = 'slashcommand_description';
    public providesPreview = false;

    // imported to access the public method from the main class
    constructor(private readonly app: GoogleCalendarApp) {}

    public async executor(context: SlashCommandContext, read: IRead, modify: IModify, http: IHttp, persistence: IPersistence): Promise<void> {
        const command = this.getCommandFromContextArguments(context);

        if (!command) {
            return await this.displayAppHelpMessage(read, modify, context.getSender(), context.getRoom());
        }

        switch (command) {
            case Subcommands.Login:
            case Subcommands.Auth:
            case Subcommands.Authorize:
                await authorize(this.app, read, modify, context.getSender(), persistence);
                break;

            case Subcommands.Revoke:
            case Subcommands.Logout:
            case Subcommands.Unauth:
            case Subcommands.Unauthorize:
                await revoke(this.app, read, modify, persistence, context.getSender());
                break;

            case Subcommands.CheckForUserEvents:
                await checkForUserEvents({}, read, modify, http, persistence);
                break;

            case Subcommands.Help:
            default:
                await this.displayAppHelpMessage(read, modify, context.getSender(), context.getRoom());
                break;
        }
    }

    private getCommandFromContextArguments(context: SlashCommandContext): string {
        const [command] = context.getArguments();
        return command;
    }

    private async displayAppHelpMessage(read: IRead, modify: IModify, user: IUser, room: IRoom): Promise<void> {
        const text = `This is all the options you have with Google Calendar App slash command /google-calendar:
*help:* shows this list;
*authorize:* starts the process to link your Google Account;
*unauthorize:* removes your linked Google Account;
        `;

        return sendNotification(read, modify, user, room, text);
    }
}
