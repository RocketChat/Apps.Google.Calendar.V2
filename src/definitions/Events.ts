export interface ITodayEvent {
    summary: string;
    start: string;
    end: string;
}

export interface ITodayEventList {
    title: string;
    list: ITodayEvent[]
};
