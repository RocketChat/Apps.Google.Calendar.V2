import { IOAuth2ClientOptions } from "@rocket.chat/apps-engine/definition/oauth2/IOAuth2";

export const oauth2Config = {
    alias: 'google-calendar-app',
    accessTokenUri: 'https://oauth2.googleapis.com/token',
    authUri: 'https://accounts.google.com/o/oauth2/auth',
    refreshTokenUri: 'https://outh2.googleapis.com/token',
    revokeTokenUri: 'https://outh2.googleapis.com/revoke',
    defaultScopes: [
        'https://www.googleapis.com/auth/calendar.readonly',
        'https://www.googleapis.com/auth/calendar.events.readonly',
    ],
} as IOAuth2ClientOptions ;
