export enum Subcommands {
    Help = 'help',
    Login = 'login',
    Auth = 'auth',
    Authorize = 'authorize',
    Logout = 'logout',
    Unauth = 'unauth',
    Unauthorize = 'unauthorize',
    Revoke = 'revoke',
    CheckForUserEvents = 'events',
};
