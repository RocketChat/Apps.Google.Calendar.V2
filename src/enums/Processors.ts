export enum Processors {
    GoogleCalendarAllEvents = 'googleCalendarAllEvents',
    GoogleCalendarEvent = 'googleCalendarEvent',
    CheckForUserEvents = 'checkForUserEvents',
}
