export enum GoogleCalendarEndpoints {
    CalendarList = 'https://www.googleapis.com/calendar/v3/users/me/calendarList',
    CalendarEvents = 'https://www.googleapis.com/calendar/v3/calendars/CALENDARID/events',
}
