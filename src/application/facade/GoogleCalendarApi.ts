import { IHttp } from '@rocket.chat/apps-engine/definition/accessors';
const moment = require('moment');

import { GoogleCalendarEndpoints } from '../../enums/GoogleCalendarEndpoints';
import { ICalendarList, IEventsList } from './@types/IGoogleCalendarApi';

export default class GoogleCalendarAPI {
    private http: IHttp;
    constructor(http: IHttp) {
        this.http = http;
    }

    public async getCalendarList(accessToken: string): Promise<ICalendarList> {
        const url = GoogleCalendarEndpoints.CalendarList + '?minAccessRole=owner';

        const response = await this.http.get(url, {
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            }
        });

        if (response.statusCode !== 200) {
            throw new Error(`Failed to get the user's calendar list: ${JSON.stringify(response.data, null, 2)}`);
        }

        return response.data as ICalendarList;
    }

    public async getEventsList(calendarId: string, accessToken: string): Promise<IEventsList> {
        const url = GoogleCalendarEndpoints.CalendarEvents
        .replace('CALENDARID', calendarId)
        + `?timeMin=${getStartOfDay()}&timeMax=${getEndOfDay()}`;

        const response = await this.http.get(url, {
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            }
        });

        if (response.statusCode !== 200) {
            throw new Error(`Failed to get the user's event list: ${response.data}`);
        }

        return response.data as IEventsList;
    }
}

function getStartOfDay(): string {
    return moment().startOf('day').toISOString();
}

function getEndOfDay(): string {
    return moment().endOf('day').toISOString();
}
