// tslint:disable: quotemark

// Method: GET
// URL: https://www.googleapis.com/calendar/v3/users/me/calendarList
// Return Type: ICalendarList
export const calendarList = {
    kind: "calendar#calendarList",
    etag: '"p32sf7gtaoi4va0g"',
    nextSyncToken: "CLjzw6rEifUCEhlhbGxhbi5yaWJlaXJvQHJvY2tldC5jaGF0",
    items: [
        {
            kind: "calendar#calendarListEntry",
            etag: '"1637030038608000"',
            id: "allan.ribeiro@rocket.chat",
            summary: "allan.ribeiro@rocket.chat",
            timeZone: "America/Sao_Paulo",
            colorId: "14",
            backgroundColor: "#9fe1e7",
            foregroundColor: "#000000",
            selected: true,
            accessRole: "owner",
            defaultReminders: [
                {
                    method: "popup",
                    minutes: 10,
                },
            ],
            notificationSettings: {
                notifications: [
                    {
                        type: "eventCreation",
                        method: "email",
                    },
                    {
                        type: "eventChange",
                        method: "email",
                    },
                    {
                        type: "eventCancellation",
                        method: "email",
                    },
                    {
                        type: "eventResponse",
                        method: "email",
                    },
                ],
            },
            primary: true,
            conferenceProperties: {
                allowedConferenceSolutionTypes: ["hangoutsMeet"],
            },
        },
        {
            kind: "calendar#calendarListEntry",
            etag: '"1637030038608000"',
            id: "en.brazilian#holiday@group.v.calendar.google.com",
            summary: "Holidays in Brazil",
            description: "Holidays and Observances in Brazil",
            timeZone: "America/Sao_Paulo",
            colorId: "8",
            backgroundColor: "#16a765",
            foregroundColor: "#000000",
            selected: true,
            accessRole: "reader",
            defaultReminders: [],
            conferenceProperties: {
                allowedConferenceSolutionTypes: ["hangoutsMeet"],
            },
        },
        {
            kind: "calendar#calendarListEntry",
            etag: '"1637030041230000"',
            id: "addressbook#contacts@group.v.calendar.google.com",
            summary: "Birthdays",
            description:
                "Displays birthdays, anniversaries, and other event dates of people in Google Contacts.",
            timeZone: "America/Sao_Paulo",
            colorId: "13",
            backgroundColor: "#92e1c0",
            foregroundColor: "#000000",
            selected: true,
            accessRole: "reader",
            defaultReminders: [],
            conferenceProperties: {
                allowedConferenceSolutionTypes: ["hangoutsMeet"],
            },
        },
    ],
};

// Method: GET
// URL: https://www.googleapis.com/calendar/v3/calendars/calendarId/events
// Return Type: IEventsList
export const eventList = {
    kind: "calendar#events",
    etag: '"p32ocvinrni3va0g"',
    summary: "allan.ribeiro@rocket.chat",
    updated: "2021-12-28T21:50:32.110Z",
    timeZone: "America/Sao_Paulo",
    accessRole: "owner",
    defaultReminders: [
        {
            method: "popup",
            minutes: 10,
        },
    ],
    nextSyncToken: "CLDPyvu8h_UCELDPyvu8h_UCGAUg1efixgE=",
    items: [
        {
            kind: "calendar#event",
            etag: '"3274169721672000"',
            id: "o2h99mp82ipl2ics6m57rajvu2",
            status: "cancelled",
        },
        {
            kind: "calendar#event",
            etag: '"3279490895526000"',
            id: "o2h99mp82ipl2ics6m57rajvu2_R20210831T124500",
            status: "confirmed",
            htmlLink:
                "https://www.google.com/calendar/event?eid=bzJoOTltcDgyaXBsMmljczZtNTdyYWp2dTJfMjAyMTA4MzFUMTI0NTAwWiBhbGxhbi5yaWJlaXJvQHJvY2tldC5jaGF0",
            created: "2021-03-11T14:08:49.000Z",
            updated: "2021-12-17T12:50:47.763Z",
            summary: "[Apps-Engine] Daily Sync",
            description: "Team daily sync. Doesn't happen on Retro Fridays",
            creator: {
                email: "douglas.gubert@rocket.chat",
            },
            organizer: {
                email: "douglas.gubert@rocket.chat",
            },
            start: {
                dateTime: "2021-08-31T09:45:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            end: {
                dateTime: "2021-08-31T10:00:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            recurrence: ["RRULE:FREQ=WEEKLY;BYDAY=FR,MO,TH,TU,WE"],
            iCalUID: "o2h99mp82ipl2ics6m57rajvu2_R20210831T124500@google.com",
            sequence: 1,
            attendees: [
                {
                    email: "thassio.carvalho@rocket.chat",
                    displayName: "Thassio Carvalho",
                    responseStatus: "accepted",
                },
                {
                    email: "douglas.gubert@rocket.chat",
                    organizer: true,
                    responseStatus: "accepted",
                },
                {
                    email: "aline.nunes@rocket.chat",
                    responseStatus: "tentative",
                },
                {
                    email: "vinicius.marino@rocket.chat",
                    optional: true,
                    responseStatus: "accepted",
                },
                {
                    email: "allan.ribeiro@rocket.chat",
                    self: true,
                    responseStatus: "accepted",
                },
            ],
            hangoutLink: "https://meet.google.com/dca-aurn-fzb",
            conferenceData: {
                entryPoints: [
                    {
                        entryPointType: "video",
                        uri: "https://meet.google.com/dca-aurn-fzb",
                        label: "meet.google.com/dca-aurn-fzb",
                    },
                    {
                        regionCode: "US",
                        entryPointType: "phone",
                        uri: "tel:+1-475-277-0063",
                        label: "+1 475-277-0063",
                        pin: "174707865",
                    },
                ],
                conferenceSolution: {
                    key: {
                        type: "hangoutsMeet",
                    },
                    name: "Google Meet",
                    iconUri:
                        "https://fonts.gstatic.com/s/i/productlogos/meet_2020q4/v6/web-512dp/logo_meet_2020q4_color_2x_web_512dp.png",
                },
                conferenceId: "dca-aurn-fzb",
                signature: "AGirE/LHc5+fpqsXTdWcUhH/mtGQ",
            },
            reminders: {
                useDefault: true,
            },
            eventType: "default",
        },
        {
            kind: "calendar#event",
            etag: '"3279560130570000"',
            id: "659tk3g076d5r36jdji6h0sbj8",
            status: "confirmed",
            htmlLink:
                "https://www.google.com/calendar/event?eid=NjU5dGszZzA3NmQ1cjM2amRqaTZoMHNiajggYWxsYW4ucmliZWlyb0Byb2NrZXQuY2hhdA",
            created: "2021-12-11T05:38:28.000Z",
            updated: "2021-12-17T22:27:45.285Z",
            summary: "BoardX Demo and Meet the Team",
            description:
                "Please use BBB link  -   https://bbb.rocket.chat/b/adm-1ki-nmm-bfc\n\nCome see a live demo of the BoardX shared whiteboard project, explore how it integrates into Rocket.Chat,  and meet the BoardX team. \n\nhttps://www.youtube.com/watch?v=dntgDPxJ4RQ",
            location: "https://bbb.rocket.chat/b/adm-1ki-nmm-bfc",
            creator: {
                email: "sing.li@rocket.chat",
                displayName: "Sing Li",
            },
            organizer: {
                email: "sing.li@rocket.chat",
                displayName: "Sing Li",
            },
            start: {
                dateTime: "2021-12-15T12:30:00-03:00",
                timeZone: "America/Toronto",
            },
            end: {
                dateTime: "2021-12-15T12:55:00-03:00",
                timeZone: "America/Toronto",
            },
            iCalUID: "659tk3g076d5r36jdji6h0sbj8@google.com",
            sequence: 0,
            attendees: [
                {
                    email: "sing.li@rocket.chat",
                    displayName: "Sing Li",
                    organizer: true,
                    responseStatus: "accepted",
                },
                {
                    email: "gabriel.engel@rocket.chat",
                    displayName: "Gabriel Engel",
                    responseStatus: "accepted",
                },
                {
                    email: "team@rocket.chat",
                    displayName: "All Team",
                    responseStatus: "needsAction",
                },
                {
                    email: "allan.ribeiro@rocket.chat",
                    self: true,
                    responseStatus: "accepted",
                },
                {
                    email: "usam@boardx.us",
                    responseStatus: "accepted",
                },
                {
                    email: "danny@boardx.us",
                    responseStatus: "needsAction",
                },
            ],
            hangoutLink: "https://meet.google.com/icf-zgaz-sug",
            conferenceData: {
                entryPoints: [
                    {
                        entryPointType: "video",
                        uri: "https://meet.google.com/icf-zgaz-sug",
                        label: "meet.google.com/icf-zgaz-sug",
                    },
                    {
                        entryPointType: "more",
                        uri: "https://tel.meet/icf-zgaz-sug?pin=9878916928951",
                        pin: "9878916928951",
                    },
                    {
                        regionCode: "CA",
                        entryPointType: "phone",
                        uri: "tel:+1-778-728-9244",
                        label: "+1 778-728-9244",
                        pin: "580076594",
                    },
                ],
                conferenceSolution: {
                    key: {
                        type: "hangoutsMeet",
                    },
                    name: "Google Meet",
                    iconUri:
                        "https://fonts.gstatic.com/s/i/productlogos/meet_2020q4/v6/web-512dp/logo_meet_2020q4_color_2x_web_512dp.png",
                },
                conferenceId: "icf-zgaz-sug",
                signature: "AGirE/LOIeq+YQtH+M4DcVXYY453",
            },
            reminders: {
                useDefault: true,
            },
            eventType: "default",
        },
        {
            kind: "calendar#event",
            etag: '"3279560141788000"',
            id: "4pkv52np5uhcq7m1pve5igfkud",
            status: "confirmed",
            htmlLink:
                "https://www.google.com/calendar/event?eid=NHBrdjUybnA1dWhjcTdtMXB2ZTVpZ2ZrdWQgYWxsYW4ucmliZWlyb0Byb2NrZXQuY2hhdA",
            created: "2021-11-12T14:30:59.000Z",
            updated: "2021-12-17T22:27:50.894Z",
            summary:
                "Rocket.Chats - A revolutionary approach to conversations that matter",
            description:
                "\u003cp\u003e\u003cspan\u003eLet’s talk about conversations that get results and build relationships&nbsp;\u003c/span\u003e\u003c/p\u003e\u003cp\u003e\u003cspan\u003eUndo old assumptions, spark new insights and lay the groundwork for transformation across your organization.\u003c/span\u003e\u003c/p\u003e\u003cp\u003e\u003cspan\u003eAre you bogged down in conversations that go nowhere? Are employees spinning their wheels in meetings that accomplish nothing?\u003c/span\u003e\u003c/p\u003e\u003cp\u003e\u003cspan\u003ePointless conversations and useless meetings are stalling your success.\u003c/span\u003e\u003c/p\u003e\u003cp\u003e\u003cspan\u003eYou can’t afford another pointless conversation.\u003c/span\u003e\u003c/p\u003e",
            creator: {
                email: "laura.coutinho@rocket.chat",
            },
            organizer: {
                email: "laura.coutinho@rocket.chat",
            },
            start: {
                dateTime: "2021-12-15T14:00:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            end: {
                dateTime: "2021-12-15T14:45:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            iCalUID: "4pkv52np5uhcq7m1pve5igfkud@google.com",
            sequence: 2,
            attendees: [
                {
                    email: "muni.narayan@rocket.chat",
                    displayName: "Muni Narayan",
                    responseStatus: "accepted",
                },
                {
                    email: "laura.coutinho@rocket.chat",
                    organizer: true,
                    responseStatus: "accepted",
                },
                {
                    email: "team@rocket.chat",
                    displayName: "All Team",
                    responseStatus: "needsAction",
                },
                {
                    email: "allan.ribeiro@rocket.chat",
                    self: true,
                    responseStatus: "accepted",
                },
            ],
            hangoutLink: "https://meet.google.com/obo-hxwm-ctu",
            conferenceData: {
                entryPoints: [
                    {
                        entryPointType: "video",
                        uri: "https://meet.google.com/obo-hxwm-ctu",
                        label: "meet.google.com/obo-hxwm-ctu",
                    },
                    {
                        entryPointType: "more",
                        uri: "https://tel.meet/obo-hxwm-ctu?pin=9481590365925",
                        pin: "9481590365925",
                    },
                    {
                        regionCode: "BR",
                        entryPointType: "phone",
                        uri: "tel:+55-41-4560-9831",
                        label: "+55 41 4560-9831",
                        pin: "756178561",
                    },
                ],
                conferenceSolution: {
                    key: {
                        type: "hangoutsMeet",
                    },
                    name: "Google Meet",
                    iconUri:
                        "https://fonts.gstatic.com/s/i/productlogos/meet_2020q4/v6/web-512dp/logo_meet_2020q4_color_2x_web_512dp.png",
                },
                conferenceId: "obo-hxwm-ctu",
                signature: "AGirE/JCi84lgLsh02H0yxpNLias",
            },
            reminders: {
                useDefault: true,
            },
            eventType: "default",
        },
        {
            kind: "calendar#event",
            etag: '"3281456370260000"',
            id: "gp7m4j4j3nipd40242118votsn_20211214T140000Z",
            status: "confirmed",
            htmlLink:
                "https://www.google.com/calendar/event?eid=Z3A3bTRqNGozbmlwZDQwMjQyMTE4dm90c25fMjAyMTEyMTRUMTQwMDAwWiBhbGxhbi5yaWJlaXJvQHJvY2tldC5jaGF0",
            created: "2018-10-25T16:38:09.000Z",
            updated: "2021-12-28T21:49:45.130Z",
            summary: "Rocket.Chat Weekly Call",
            description:
                "\u003cb\u003eWeekly Update Order\u003c/b\u003e\u003cbr\u003e\u003cbr\u003eGeneral Management\u003cbr\u003eOperations\u003cbr\u003ePeople\u003cbr\u003eSales\u003cbr\u003eMarketing\u003cbr\u003eDesign\u003cbr\u003eFront-end\u003cbr\u003eMobile\u003cbr\u003eBack-end\u003cbr\u003eOmnichannel\u003cbr\u003eCloud\u003cbr\u003eApps-Engine\u003cbr\u003eSecurity\u003cbr\u003eCustomer Success\u003cbr\u003eEnterprise Solutions\u003cbr\u003eCommunity Opportunities\u003cbr\u003eCTO\u003cbr\u003eCEO",
            location: "https://bbb.rocket.chat/b/adm-1ki-nmm-bfc",
            creator: {
                email: "marcelo.schmidt@rocket.chat",
                displayName: "Marcelo Schmidt",
            },
            organizer: {
                email: "marcelo.schmidt@rocket.chat",
                displayName: "Marcelo Schmidt",
            },
            start: {
                dateTime: "2021-12-15T11:00:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            end: {
                dateTime: "2021-12-15T12:00:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            recurringEventId: "gp7m4j4j3nipd40242118votsn_R20211214T140000",
            originalStartTime: {
                dateTime: "2021-12-14T11:00:00-03:00",
                timeZone: "America/Sao_Paulo",
            },
            iCalUID: "gp7m4j4j3nipd40242118votsn_R20211214T140000@google.com",
            sequence: 6,
            attendees: [
                {
                    email: "allan.ribeiro@rocket.chat",
                    self: true,
                    responseStatus: "accepted",
                },
            ],
            guestsCanInviteOthers: false,
            guestsCanSeeOtherGuests: false,
            reminders: {
                useDefault: true,
            },
            eventType: "default",
        },
    ],
};
