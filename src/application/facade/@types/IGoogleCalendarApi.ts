export interface IDefaultReminder {
    method: string;
    minutes: number;
}

export interface ICreator {
    email: string;
    self?: boolean;
    displayName: string;
}

export interface IOrganizer {
    email: string;
    self?: boolean;
    displayName: string;
}

export interface IStart {
    dateTime: Date;
    timeZone: string;
    date: string;
}

export interface IEnd {
    dateTime: Date;
    timeZone: string;
    date: string;
}

export interface IAttendee {
    email: string;
    responseStatus: string;
    self?: boolean;
    organizer?: boolean;
    optional?: boolean;
    comment: string;
    displayName: string;
}

export interface IEntryPoint {
    entryPointType: string;
    uri: string;
    label: string;
    pin: string;
    regionCode: string;
}

export interface IKey {
    type: string;
}

export interface IConferenceSolution {
    key: IKey;
    name: string;
    iconUri: string;
}

export interface IConferenceData {
    entryPoints: Array<IEntryPoint>;
    conferenceSolution: IConferenceSolution;
    conferenceId: string;
    signature: string;
}

export interface IReminders {
    useDefault: boolean;
}

export interface IOriginalStartTime {
    dateTime: Date;
    timeZone: string;
}

export interface IAttachment {
    fileUrl: string;
    title: string;
    mimeType: string;
    iconLink: string;
    fileId: string;
}

export interface IEvent {
    kind: string;
    etag: string;
    id: string;
    status: string;
    htmlLink: string;
    created: Date;
    updated: Date;
    summary: string;
    creator: ICreator;
    organizer: IOrganizer;
    start: IStart;
    end: IEnd;
    iCalUID: string;
    sequence: number;
    attendees: Array<IAttendee>;
    hangoutLink: string;
    conferenceData: IConferenceData;
    reminders: IReminders;
    eventType: string;
    recurringEventId: string;
    originalStartTime: IOriginalStartTime;
    recurrence: Array<string>;
    guestsCanModify?: boolean;
    description: string;
    attachments: Array<IAttachment>;
    transparency: string;
    location: string;
    guestsCanInviteOthers?: boolean;
    guestsCanSeeOtherGuests?: boolean;
}

export interface IEventsList {
    kind: string;
    etag: string;
    summary: string;
    updated: Date;
    timeZone: string;
    accessRole: string;
    defaultReminders: Array<IDefaultReminder>;
    nextPageToken: string;
    items: Array<IEvent>;
}

export interface IDefaultReminder {
    method: string;
    minutes: number;
}

export interface INotification {
    type: string;
    method: string;
}

export interface INotificationSettings {
    notifications: Array<INotification>;
}

export interface IConferenceProperties {
    allowedConferenceSolutionTypes: Array<string>;
}

export interface ICalendar {
    kind: string;
    etag: string;
    id: string;
    summary: string;
    timeZone: string;
    colorId: string;
    backgroundColor: string;
    foregroundColor: string;
    selected: boolean;
    accessRole: string;
    defaultReminders: Array<IDefaultReminder>;
    notificationSettings: INotificationSettings;
    primary: boolean;
    conferenceProperties: IConferenceProperties;
    description: string;
}

export interface ICalendarList {
    kind: string;
    etag: string;
    nextSyncToken: string;
    items: Array<ICalendar>;
}
