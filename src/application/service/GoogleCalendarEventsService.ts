import { IHttp } from '@rocket.chat/apps-engine/definition/accessors';
import { ITodayEventList } from '../../definitions/Events';
import {
    IAttendee,
    ICalendar,
    ICreator,
    IEventsList,
} from '../facade/@types/IGoogleCalendarApi';
import GoogleCalendarApi from '../facade/GoogleCalendarApi';

export default class GoogleCalendarEventsService {
    private api: GoogleCalendarApi;

    constructor(http: IHttp) {
        this.api = new GoogleCalendarApi(http);
    }

    public async getGoogleCalendarEvents() {
        const eventsList = await this.getCalendarEventsList();

        return this.parseEvents(eventsList);
    }

    public async getGoogleCalendarEvent(eventTimeInAdvanceOffSet: number) {
        const eventsList = await this.getCalendarEventsList();
        return this.parseEvent(eventsList);
    }

    /**
      * Gets the list of today's events of a given user (represented by their accessToken)
      */
    public async getEventsForToday(accessToken: string): Promise<ITodayEventList | undefined> {
        const calendarList = await this.api.getCalendarList(accessToken);

        const { items } = calendarList;

        if (!items.length) {
            // nothing to do here
            return;
        }

        const promisedEventLists = items.map(async (calendar: ICalendar) => this.api.getEventsList(calendar.id, accessToken));

        // each calendar retrieved has an event list
        const eventLists = await Promise.all(promisedEventLists);

        const events = eventLists.reduce((acc, curr) => [...acc, ...curr.items], []);

        if (!events) {
            return;
        }

        return this.parseEvents({ ...eventLists[0], items: events });
    }

    private parseDateToList(date: Date, timeZone: string): string {
        return new Date(date)
            .toLocaleDateString('en-US', {
                hour: 'numeric',
                timeZone,
            })
            .split(',')[1];
    }

    private parseEvents(events: IEventsList) {
        const timezone = events.timeZone;

        const today = new Date().toLocaleDateString('en-US', {
            weekday: 'long',
            month: 'long',
            day: 'numeric',
            year: 'numeric',
            timeZone: timezone,
        });

        return {
            title: `*Today* - ${today}`,
            list: events.items
                .filter((item) => item.status !== 'cancelled')
                .map((item) => ({
                    summary: `[${item.summary}](${item.htmlLink})`,
                    start: this.parseDateToList(
                        item.start.dateTime,
                        item.start.timeZone,
                    ),
                    end: this.parseDateToList(
                        item.end.dateTime,
                        item.end.timeZone,
                    ),
                })),
        };
    }

    private async getCalendarEventsList() {
        // const calendarList = await this.api.getCalendarList();
        const calendarId = this.getCalendarId();
        const eventsList = await this.api.getEventsList(calendarId, '');
        return eventsList;
    }

    private organizeGuests(creator: ICreator, attendees: Array<IAttendee>) {
        const attendeesNames = attendees
            .filter(
                // tslint:disable-next-line: no-shadowed-variable
                (attendees: IAttendee) =>
                    attendees.displayName !== creator.displayName &&
                    !attendees.self,
            )
            .map(
                // tslint:disable-next-line: no-shadowed-variable
                (attendees: IAttendee) =>
                    attendees.displayName ?? attendees.email,
            );

        if (attendeesNames.length <= 4) {
            return `${creator.displayName} (organizer),${attendeesNames.toString()}`
                .replace(
                new RegExp(/\,/, 'g'), ', ');
        }

        return `${creator.displayName} (organizer),${attendeesNames[0]},${
            attendeesNames[1]
        },${attendeesNames[2]}&${attendeesNames.length - 4}
        `.replace(new RegExp(/\,/, 'g'), ', ');
    }

    private parseEvent(events: IEventsList) {
        const { items, timeZone } = events;

        const today = new Date().toLocaleDateString('en-US', {
            weekday: 'long',
            month: 'long',
            day: 'numeric',
            year: 'numeric',
            timeZone,
        });

        return {
            summary: items[2].summary,
            hangoutLink: items[2].htmlLink,
            day: today,
            when: `${this.parseDateToList(
                items[2].start.dateTime,
                timeZone,
            )} - ${this.parseDateToList(items[2].end.dateTime, timeZone)}`,
            where: `${items[2].location ?? items[2].hangoutLink}`,
            guests: `${this.organizeGuests(
                items[2].creator,
                items[2].attendees,
            )}`,
        };
    }

    private getCalendarId(): string {
        return 'allan.ribeiro@rocket.chat';
    }
}
