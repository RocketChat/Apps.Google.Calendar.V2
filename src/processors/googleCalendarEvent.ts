import { IHttp, IModify, IPersistence, IRead } from "@rocket.chat/apps-engine/definition/accessors";
import { IJobContext, IProcessor, StartupType } from "@rocket.chat/apps-engine/definition/scheduler";
import { IUser } from "@rocket.chat/apps-engine/definition/users";
import { GoogleCalendarApp } from "../../GoogleCalendarApp";
import GoogleCalendarEvent from "../commands/GoogleCalendarEvent";
import { Processors } from "../enums/Processors";

export function getCalendarEventProcessor(app: GoogleCalendarApp, user: IUser): IProcessor {
    const googleCalendarEvent = new GoogleCalendarEvent(app);

    return {
        id: Processors.GoogleCalendarEvent,
        startupSetting: {
            type: StartupType.RECURRING,
            interval: '58 17 * * *',
            data: { appId: app.getID() },
        },
        processor: async (_: IJobContext, read: IRead, modify: IModify, http: IHttp, persistence: IPersistence) => {
            await googleCalendarEvent.run({
                read,
                modify,
                http,
                user,
                persistence,
            });
        },
    };
}

