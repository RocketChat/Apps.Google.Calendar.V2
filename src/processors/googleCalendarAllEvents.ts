import { IHttp, IModify, IPersistence, IRead } from "@rocket.chat/apps-engine/definition/accessors";
import { IJobContext, IProcessor, StartupType } from "@rocket.chat/apps-engine/definition/scheduler";
import { IUser } from "@rocket.chat/apps-engine/definition/users";
import { GoogleCalendarApp } from "../../GoogleCalendarApp";
import { GoogleCalendarEvents } from "../commands/GoogleCalendarEvents";
import { Processors } from "../enums/Processors";

export function getCalendarAllEventsProcessor(app: GoogleCalendarApp, user: IUser): IProcessor {
    const googleCalendarEvents = new GoogleCalendarEvents(app);

    return {
        id: Processors.GoogleCalendarAllEvents,
        startupSetting: {
            type: StartupType.RECURRING,
            interval: '20 17 * * *',
            data: { appId: app.getID() },
        },
        processor: async (_: IJobContext, read: IRead, modify: IModify, http: IHttp, persistence: IPersistence) => {
            await googleCalendarEvents.run({
                read,
                modify,
                http,
                user,
                persistence,
            });
        },
    };
}
