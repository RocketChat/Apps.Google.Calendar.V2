import { IHttp, IModify, IPersistence, IRead } from "@rocket.chat/apps-engine/definition/accessors";
import { IAuthData } from "@rocket.chat/apps-engine/definition/oauth2/IOAuth2";
import { IJobContext, IProcessor, StartupType } from "@rocket.chat/apps-engine/definition/scheduler";
import { IUser } from "@rocket.chat/apps-engine/definition/users";
const moment = require('moment');

import GoogleCalendarEventsService from "../application/service/GoogleCalendarEventsService";
import { oauth2Config } from "../definitions/OAuth2Config";
import { Processors } from "../enums/Processors";
import { formatEventsForToday } from "../lib/eventUtils";
import { sendDirectMessage } from "../lib/message";

import {
    getAccessTokenForUser,
    getAllUsers as getRegisteredUserIds,
    remove as removeRegisteredUserId,
} from "../storage/users";

type IUserWithToken = IUser & { token: string };

export function getCheckForUserEventsProcessor(): IProcessor {
    return {
        id: Processors.CheckForUserEvents,
        startupSetting: {
            type: StartupType.RECURRING,
            // every hour
            interval: '0 * * * *'
        },
        processor: checkForUserEvents,
    }
}

export async function checkForUserEvents(_: IJobContext, read: IRead, modify: IModify, http: IHttp, persistence: IPersistence): Promise<void> {
    const users = await getAuthorizedUsers(read);

    const usersToNotify = users.filter(isUserNotificationWindow);

    if (!usersToNotify.length) {
        // nothing to do here
        return;
    }

    const promisedUsersWithToken = usersToNotify.map(async (user: IUser): Promise<IUserWithToken | undefined> => {
        const authData = await getUserAuthData(read, persistence, user);

        if (!authData) {
            return;
        }

        return { ...user, token: authData.token };
    });

    const usersWithToken = await Promise.all(promisedUsersWithToken);

    const calendarEventsService = new GoogleCalendarEventsService(http);

    const promisedNotifs = usersWithToken.map(async (user: IUserWithToken) => {
        const eventsForToday = await calendarEventsService.getEventsForToday(user.token)

        if (eventsForToday) {
            const formattedEvents = formatEventsForToday(eventsForToday);
            return sendDirectMessage(read, modify, user, formattedEvents, persistence);
        }
        return;
    });

    await Promise.all(promisedNotifs);
}

async function getAuthorizedUsers(read: IRead): Promise<IUser[]> {
     return getRegisteredUserIds(read);
}

async function getUserAuthData(read: IRead, persistence: IPersistence, user: IUser): Promise<IAuthData | void> {
    const data = await getAccessTokenForUser(read, user, oauth2Config);

    if (!data) {
        // @NOTE the user revoked their credentials, so we should remove it from our list
        return removeRegisteredUserId(read, persistence, user);
    }

    return data;

    // @NOTE to filter out empty values for when there's a credential removal
    // return authData.filter(ad => ad);
}

function isUserNotificationWindow(user: IUser): boolean {
    const time = moment().utcOffset(user.utcOffset);

    // "is it 5 am where they're at?"
    if (time.hour() === 5) {
        return true;
    }

    return false;
}
