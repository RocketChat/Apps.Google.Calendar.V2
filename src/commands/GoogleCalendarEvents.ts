import {
    IHttp,
    IModify,
    IPersistence,
    IRead,
} from '@rocket.chat/apps-engine/definition/accessors';
import { ButtonStyle } from '@rocket.chat/apps-engine/definition/uikit';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../GoogleCalendarApp';
import { IMessageButonActions } from '../../IGoogleCalendarApp';
import GoogleCalendarEventsService from '../application/service/GoogleCalendarEventsService';
import { sendDirectMessage } from '../lib/message';

export class GoogleCalendarEvents {

    constructor(private readonly app: GoogleCalendarApp) {}

    public async run({
        read,
        modify,
        http,
        user,
        persistence,
    }: {
        read: IRead;
        modify: IModify;
        http: IHttp;
        user: IUser;
        persistence: IPersistence;
    }): Promise<void> {
        let text = '';

        const blocks = modify.getCreator().getBlockBuilder();

        const service = new GoogleCalendarEventsService(http);

        const events = await service.getGoogleCalendarEvents();

        text = `${events.title} \n ${events.list
            .map((list) => `${list.start} - ${list.end} - ${list.summary}\n`)
            .join('')}`;

        blocks.addSectionBlock({
            text: blocks.newMarkdownTextObject(text),
        });

        blocks.addActionsBlock({
            elements: [
                blocks.newButtonElement({
                    actionId: IMessageButonActions.GoToSettings,
                    text: blocks.newPlainTextObject('Settings'),
                    style: ButtonStyle.PRIMARY,
                }),
            ],
        });

        await sendDirectMessage(read, modify, user, text, persistence, blocks);
    }
}
