import {
    IHttp,
    IModify,
    IPersistence,
    IRead,
} from '@rocket.chat/apps-engine/definition/accessors';

import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { GoogleCalendarApp } from '../../GoogleCalendarApp';
import { IMessageButonActions } from '../../IGoogleCalendarApp';
import GoogleCalendarEvents from '../application/service/GoogleCalendarEventsService';
import { sendDirectMessage } from '../lib/message';

export default class GoogleCalendarEvent {

    constructor(private readonly app: GoogleCalendarApp) {}

    private set eventTime(eventTime: number) {
        this.eventTime = eventTime;
    }

    public get eventTime(): number {
        return this.eventTime;
    }

    public async run({
        read,
        modify,
        http,
        user,
        persistence,
    }: {
        read: IRead;
        modify: IModify;
        http: IHttp;
        user: IUser;
        persistence: IPersistence;
    }): Promise<void> {
        let text = '';
        const blocks = modify.getCreator().getBlockBuilder();

        const service = new GoogleCalendarEvents(http);

        const {  summary, when, where, guests, day, hangoutLink } =
            await service.getGoogleCalendarEvent(10);

        text = `This event will start in 10 minutes
                :calendar_spiral: [${summary}](${hangoutLink})
                *When:* ${day} at ${when}
                *Where:* ${where}
                *Guests:* ${guests}`;

        blocks.addSectionBlock({
            text: blocks.newMarkdownTextObject(text),
        });

        blocks.addActionsBlock({
            elements: [
                blocks.newButtonElement({
                    actionId: IMessageButonActions.GoToMeeting,
                    text: blocks.newPlainTextObject('Join the Meeting'),
                    url: where,
                }),
            ],
        });

        await sendDirectMessage(read, modify, user, text, persistence, blocks);
    }
}
