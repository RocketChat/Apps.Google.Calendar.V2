import {
    IAppAccessors,
    IAppInstallationContext,
    IConfigurationExtend,
    IHttp,
    ILogger,
    IModify,
    IPersistence,
    IRead,
} from '@rocket.chat/apps-engine/definition/accessors';
import { App } from '@rocket.chat/apps-engine/definition/App';
import { IAppInfo } from '@rocket.chat/apps-engine/definition/metadata';
import { IAuthData, IOAuth2Client, IOAuth2ClientOptions } from '@rocket.chat/apps-engine/definition/oauth2/IOAuth2';
import { createOAuth2Client } from '@rocket.chat/apps-engine/definition/oauth2/OAuth2';
import {
    ButtonStyle,
    IUIKitInteractionHandler,
    IUIKitResponse,
    UIKitBlockInteractionContext,
} from '@rocket.chat/apps-engine/definition/uikit';
import { IUser } from '@rocket.chat/apps-engine/definition/users';
import { IMessageButonActions } from './IGoogleCalendarApp';
import { oauth2Config } from './src/definitions/OAuth2Config';
import GoogleCalendarEvent from './src/commands/GoogleCalendarEvent';
import { GoogleCalendarEvents } from './src/commands/GoogleCalendarEvents';
import { IProcessors } from './src/enums/Processors';
import { createSectionBlock } from './src/lib/blocks';
import { contextualBarOptions } from './src/lib/contextualBar';
import { isUserHighHierarchy, sendDirectMessage } from './src/lib/message';
import { NotificationsController } from './src/lib/notifications';
import { ViewController } from './src/lib/view';
import { getCheckForUserEventsProcessor } from './src/processors/checkForUserEvents';
import { getCalendarAllEventsProcessor } from './src/processors/googleCalendarAllEvents';
import { getCalendarEventProcessor } from './src/processors/googleCalendarEvent';
import { GoogleCalendar as GoogleCalendarCommand } from './src/slashcommands/googleCalendar';
import { revoke } from './src/slashcommands/subcommands/revoke';
import { create as registerAuthorizedUser } from './src/storage/users';

export class GoogleCalendarApp extends App implements IUIKitInteractionHandler {
    /**
     * The bot username who sends the messages
     */
    public botUsername: string;

    /**
     * The bot user sending messages
     */
    public botUser: IUser;

    private oauth2Config: IOAuth2ClientOptions = {
        alias: 'google-calendar-app',
        accessTokenUri: 'https://oauth2.googleapis.com/token',
        authUri: 'https://accounts.google.com/o/oauth2/auth',
        refreshTokenUri: 'https://outh2.googleapis.com/token',
        revokeTokenUri: 'https://outh2.googleapis.com/revoke',
        defaultScopes: ['https://www.googleapis.com/auth/calendar.events.readonly'],
        authorizationCallback: this.autorizationCallback.bind(this),
    };

    private oauth2ClientInstance: IOAuth2Client;

    constructor(info: IAppInfo, logger: ILogger, accessors: IAppAccessors) {
        super(info, logger, accessors);
        this.oauth2Config = {
            ...oauth2Config,
        };
    }

    public async initialize(
        configurationExtend: IConfigurationExtend,
    ): Promise<void> {
        await this.extendConfiguration(configurationExtend);
        this.getLogger().log('Google Calendar initialized');
    }

    public async onEnable(): Promise<boolean> {
        this.botUsername = 'google-calendar.bot';
        this.botUser = (await this.getAccessors()
            .reader.getUserReader()
            .getByUsername(this.botUsername)) as IUser;
        return true;
    }

    public async executeBlockActionHandler(
        context: UIKitBlockInteractionContext,
        read: IRead,
        _: IHttp,
        persistence: IPersistence,
        modify: IModify,
    ): Promise<IUIKitResponse> {
        try {
            const data = context.getInteractionData();

            const { actionId, triggerId, user, container, blockId} = data;

            const viewController = new ViewController(this, persistence, read, container.id as string, user);
            const notificationsController = new NotificationsController(read, persistence, user);

            switch (actionId) {
                case IMessageButonActions.GoToSettings: {
                    const view = await contextualBarOptions({
                        id: container.id,
                        read,
                        modify,
                        persistence,
                        app: this,
                        user,
                    });

                    await modify.getUiController().openContextualBarView(view, { triggerId }, user);

                    return {
                        success: true,
                    };

                }
                case IMessageButonActions.GoToMeeting: {
                    return {
                        success: true,
                    };
                }

                case IMessageButonActions.RevokeGoogleOAuth: {
                    const newLinkViewUpdate = await viewController.toggleButtonsView(
                        blockId,
                    );

                    await modify
                        .getUiController()
                        .updateContextualBarView(
                            newLinkViewUpdate,
                            { triggerId },
                            user,
                        );

                    await revoke(this, read, modify, persistence, user);

                    return context.getInteractionResponder().successResponse();
                }

                case IMessageButonActions.RemoveNotifications: {

                    const newLinkViewUpdate = await viewController.toggleButtonsView(
                        blockId,
                    );

                    await Promise.all([
                        modify
                        .getUiController()
                        .updateContextualBarView(
                            newLinkViewUpdate,
                            { triggerId },
                            user,
                        ),
                        notificationsController.updateNotificationsStatus(false),
                    ]);

                    return context.getInteractionResponder().successResponse();
                }

                case IMessageButonActions.AddNotifications: {
                    const newLinkViewUpdate = await viewController.toggleButtonsView(
                        blockId,
                    );

                    await Promise.all([
                        modify
                        .getUiController()
                        .updateContextualBarView(
                            newLinkViewUpdate,
                            { triggerId },
                            user,
                        ),
                        notificationsController.updateNotificationsStatus(true),
                    ]);

                    return context.getInteractionResponder().successResponse();
                }

                case IMessageButonActions.AuthGoogleOAuth: {
                    const newLinkViewUpdate = await viewController.toggleButtonsView(
                       blockId,
                    );

                    await modify
                        .getUiController()
                        .updateContextualBarView(
                            newLinkViewUpdate,
                            { triggerId },
                            user,
                        );

                    return context.getInteractionResponder().successResponse();
                }
            }

            return {
                success: true,
            };
        } catch (error) {
            this.getLogger().error(error);
            return { success: false };
        }
    }

    public getOauth2ClientInstance(): IOAuth2Client {
        if (!this.oauth2ClientInstance) {
            this.oauth2ClientInstance = createOAuth2Client(this, this.oauth2Config);
        }
        return this.oauth2ClientInstance;
    }

    public async onInstall(
        context: IAppInstallationContext,
        read: IRead,
        http: IHttp,
        persistence: IPersistence,
        modify: IModify,
    ): Promise<void> {
        const user = context.user;

        const quickReminder = 'Quick reminder: Let your workspaces users know about the App,\
                            so everyone will be able to manage their google calendar as well.\n';

        const text =
            `Welcome to the Google Calendar Rocket.Chat App!\n` +
            `To start receiving message notifications about your tasks,` +
            `you first need to complete the app's setup and then authorize your Google Calendar account.\n` +
            `To authorize your google calendar account, run \`/google-calendar auth\`\n` +
            `${isUserHighHierarchy(user) ? quickReminder : ''}`;

        await sendDirectMessage(read, modify, user, text, persistence);
    }

    protected async extendConfiguration(
        configuration: IConfigurationExtend,
    ): Promise<void> {
        const user = (await this.getAccessors()
            .reader.getUserReader()
            .getAppUser()) as IUser;

        await Promise.all([
            this.getOauth2ClientInstance().setup(configuration),
            configuration.slashCommands.provideSlashCommand(new GoogleCalendarCommand(this)),
            configuration.scheduler.registerProcessors([
                getCalendarAllEventsProcessor(this, user),
                getCalendarEventProcessor(this, user),
                getCheckForUserEventsProcessor(),
            ]),
        ]);
    }

    private async autorizationCallback(
        token: IAuthData,
        user: IUser,
        read: IRead,
        modify: IModify,
        http: IHttp,
        persistence: IPersistence,
    ) {
          
        if (token) {
            await registerAuthorizedUser(read, persis, user);
        }  
          
        const text =
        `The authentication process has succeed! :tada:\n` +
        `Tomorrow you will start to receive Direct Messages with ` +
        `your daily meeting summary and will also be notified for ` +
        `each event 10 minutes before it starts.\n` +
        `If you want to change any settings, please click the button ` +
        `below or type \`/google-calendar settings\` in the message composer.`;

        const button = {
            actionId: IMessageButonActions.GoToSettings,
            text: 'Settings',
        };

        const blocks = await createSectionBlock(modify, text, button);

        await sendDirectMessage(read, modify, user, text, persistence, blocks);
    }
}
