export enum IMessageButonActions {
    GoToSettings = 'goToSettings',
    GoToMeeting = 'goToMeeting',
    RevokeGoogleOAuth = 'revokeGoogleOAuth',
    AuthGoogleOAuth = 'AuthGoogleOAuth',
    RemoveNotifications = 'removeNotifications',
    AddNotifications = 'addNotifications',
}
